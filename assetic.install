<?php
/**
 * @file
 * Contains install and update functions for Assetic.
 */

/**
 * Implements hook_schema().
 */
function assetic_schema() {
  $schema['assetic_filter'] = array(
    'description' => 'Table that stores information about defined Assetic filters.',
    'fields' => array(
      'alias' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'Primary Key: Unique machine name of the filter.',
      ),
      'module' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The origin module of the filter.',
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Weight of filter within format.',
      ),
      'status' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Filter enabled status. (1 = enabled, 0 = disabled)',
      ),
      'settings' => array(
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of name value pairs that store the filter settings.',
      ),
    ),
    'primary key' => array('alias'),
    'indexes' => array(
      'list' => array('weight', 'module'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_requirements().
 */
function assetic_requirements($phase) {
  $requirements = array();

  // We can only check for library dependencies on runtime.
  if ($phase !== 'runtime') {
    return $requirements;
  }

  // Check if the Assetic library exists.
  $requirements['assetic_library'] = _assetic_requirements_check_library('assetic', ASSETIC_VERSION, 'Assetic\\Asset\\AssetCollection');
  // Check if the dependency for the Symfony Process component can be solved.
  $requirements['assetic_dependency_symfony_process'] = _assetic_requirements_check_library('symfony-process', ASSETIC_SYMFONY_PROCESS_VERSION, 'Symfony\\Component\\Process\\ProcessBuilder');

  return $requirements;
}

/**
 * Implements hook_uninstall().
 */
function assetic_uninstall() {
  // Remove all our settings variables.
  variable_del('assetic_debug');
}

/**
 * We are using the default cache bin from now on.
 */
function assetic_update_7100(&$sandbox) {
  db_drop_table('cache_assetic');
}

/**
 * Create a table where we store information about the filters.
 */
function assetic_update_7101(&$sandbox) {
  drupal_install_schema('assetic');
}

/**
 * Renaming 'filter' to 'alias' to match code changes.
 */
function assetic_update_7102(&$sandbox) {
  db_drop_primary_key('assetic_filter');

  $schema = assetic_schema();

  db_change_field('assetic_filter', 'filter', 'alias', $schema['assetic_filter']['fields']['alias'], array(
    'primary key' => array('alias')
  ));
}

/**
 * Add an extra field to store the serialized config of a filter.
 */
function assetic_update_7103(&$sandbox) {
  $schema = assetic_schema();

  db_add_field('assetic_filter', 'settings', $schema['assetic_filter']['fields']['settings']);
}

/**
 * Function for creating a requirements array for the given library.
 *
 * @see assetic_requirements()
 *
 * @param string $library
 *   The name of the library.
 * @param string $version
 *   A version string to compare to
 * @param string|NULL $namespace
 *   A namespace to check or NULL if the library isn't OOP.
 *
 * @return array
 *   An array with the checked requirements for the library.
 */
function _assetic_requirements_check_library($library, $version, $namespace = NULL) {
  // Load the library from the given library name.
  $library = libraries_detect($library);

  $requirements = array(
    'title' => t('Assetic - %library library', array(
      '%library' => $library['name'],
    )),
  );

  // Check if the library exists.
  if (!$library['library path']) {
    $requirements['value'] = t('Not Installed');
    $requirements['severity'] = REQUIREMENT_ERROR;
    $requirements['description'] = t("Please download the %library library (%version) from <a href='@url'>@url</a>.", array(
      '%library' => $library['name'],
      '%version' => $version,
      '@url' => $library['download url'],
    ));
  }
  // Check if the correct version of the library is installed.
  elseif (version_compare($library['version'], $version) < 0) {
    $requirements['value'] = t('Incorrect Version (%version)', array(
      '%version' => $library['version'],
    ));
    $requirements['severity'] = REQUIREMENT_ERROR;
    $requirements['description'] = t("Please use the correct library version (%version).", array(
      '%version' => $version,
    ));
  }
  // Check if the library classes are autoloaded
  // through the X Autoload module.
  elseif ($namespace && !class_exists($namespace)) {
    $requirements['value'] = t('Incorrect Autoloading');
    $requirements['severity'] = REQUIREMENT_ERROR;
    $requirements['description'] = t("Please make sure that library's classes are autoloaded through the !module module.", array(
      '!module' => l('X Autoload', 'http://drupal.org/project/xautoload'),
    ));
  }
  else {
    $requirements['value'] = t('Installed (%version)', array(
      '%version' => $library['version'],
    ));
    $requirements['severity'] = REQUIREMENT_OK;
  }

  return $requirements;
}
